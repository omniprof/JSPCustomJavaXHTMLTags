/*
 * Taken from http://www.mastertheboss.com/javaee/jsf/jsf-java-based-custom-tags-example
 */
package com.kfwebstandard.jsfcustomjavatag;

import java.io.IOException;
import javax.faces.application.FacesMessage;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is an example of a custom tag written in java
 *
 * @author Ken Fogel
 */
@FacesComponent(value = "goback")
public class GoBackComponent extends UIComponentBase {

    private final static Logger LOG = LoggerFactory.getLogger(GoBackComponent.class);

    private final String back;
    private final String back1;
    private final String back2;

    /**
     * If a custom renderer is used then the component family is used Required
     * in all cases, not being used in this example
     *
     * @return Name of component family
     */
    @Override
    public String getFamily() {
        return "goBackComponent";
    }

    /**
     * Constructor initializes strings
     */
    public GoBackComponent() {
        FacesMessage message = com.kfwebstandard.util.Messages.getMessage(
                "com.kfwebstandard.bundles.messages", "noGoBack", null);

        back = "<input type=\"button\" value=\"" + message.getDetail() + "\" onclick=\"history.back(-1)\" />";
        back1 = "<input type=\"button\" value=\"";
        back2 = "\" onclick=\"history.back(-1)\" />";
    }

    /**
     * This method is called by the tag It retrieves the attribute you require
     * such as 'value'.
     *
     * @param context of page so output can be delivered
     * @throws IOException
     */
    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        String value = (String) getAttributes().get("value");

        ResponseWriter writer = context.getResponseWriter();
        if (value == null) {
            writer.write(back);
        } else {
            writer.write(back1 + value + back2);
        }
    }
}
