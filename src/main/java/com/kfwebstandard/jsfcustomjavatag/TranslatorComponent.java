/*
 * Taken from http://www.mastertheboss.com/javaee/jsf/jsf-java-based-custom-tags-example
 */
package com.kfwebstandard.jsfcustomjavatag;

import java.io.IOException;
import java.util.HashMap;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is an example of a custom tag written in java It provides the
 * translation of one if three words into pig latin
 *
 * @author Ken Fogel
 */
@FacesComponent(value = "dictionary")
public class TranslatorComponent extends UIComponentBase {

    private final static Logger LOG = LoggerFactory.getLogger(TranslatorComponent.class);

    HashMap dictionary = new HashMap();

    /**
     * If a custom renderer is used then the component family is used Required
     * in all cases, not being used in this example
     *
     * @return Name of component family
     */
    @Override
    public String getFamily() {
        return "translatorComponent";
    }

    /**
     * Constructor that creates the three words
     */
    public TranslatorComponent() {
        dictionary.put("dog", "ogday");
        dictionary.put("cat", "atcay");
        dictionary.put("mouse", "ousemay");
    }

    /**
     * This method is called by the tag It retrieves the attribute you require
     * such as 'value'.
     *
     * @param context of page so output can be delivered
     * @throws IOException
     */
    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        String value = (String) getAttributes().get("value");

        // If there is no value then do not do nothing
        if (value != null) {

            ResponseWriter writer = context.getResponseWriter();
            Object translation = dictionary.get(value);
            if (translation == null) {
                writer.write("Sorry word not found!");
            } else {
                writer.write((String) translation);
            }
        }
    }
}
